import React, { useState } from "react";

import { FetchEditHookResult, MyData, useFetchEdit } from "./hooks";

// FetchEdit hook renderer component type
type FetchEditComponent = React.ComponentType<FetchEditHookResult<MyData>>;

// Define custom views which conform to FetchEdit component spec here
const MyViewOne: FetchEditComponent = ({ latestData }) => (
  <span>{JSON.stringify(latestData)}</span>
);

const MyViewTwo: FetchEditComponent = ({ latestData, setData }) => (
  <div>
    <ul>
      <li>/foo: {latestData?.["/foo"] ?? "undefined"}</li>
      <li>/bar: {latestData?.["/bar"] ?? "undefined"}</li>
    </ul>
    <div>
      <button
        onClick={(): void => {
          setData({
            "/foo": 3,
          });
        }}
      >
        Set /foo to 3
      </button>
    </div>
  </div>
);

type View = "one" | "two";

const MyViews: Record<View, FetchEditComponent> = {
  one: MyViewOne,
  two: MyViewTwo,
};

/*
 * In a prod app, you might have a whole UI defined here with DnD etc. which allowed
 * you to pick and mix different components.
 * In this example, I've just got a statically defined list of components which can
 * render the fetch/edit hook data, and a select box to choose which one to use.
 *
 * The driving principle to this architecture is that the data subscription and mutation
 * logic occurs separately and independently from the rendering logic
 */
export const ViewSelector: React.FC = () => {
  const [view, setView] = useState<View>("one");

  const hookResult = useFetchEdit();

  const MyView = MyViews[view];

  return (
    <div>
      <div>
        <span>How do you want to render the data?</span>
        <select
          value={view}
          onChange={(event): void => setView(event.target.value as View)}
        >
          <option value="one">One</option>
          <option value="two">Two</option>
        </select>
      </div>
      <div>
        <MyView {...hookResult} />
      </div>
    </div>
  );
};
