import { useCallback, useEffect, useMemo, useRef, useState } from "react";

export type MyData = {
  "/foo": number;
  "/bar": number | null;
};

type MessageActionSubscribe = {
  type: "SUBSCRIBE";
  paths: string[];
};

type MessageActionUnsubscribe = {
  type: "UNSUBSCRIBE";
  paths: string[];
};

type MessageActionSetValues<T extends Record<string, unknown>> = {
  type: "SET_VALUES";
  updates: Partial<T>;
};

type MessageActionValueNodes<T extends Record<string, unknown>> = {
  type: "VALUE_NODES";
  nodes: T;
};

type MessageAction =
  | MessageActionSubscribe
  | MessageActionUnsubscribe
  | MessageActionSetValues<MyData>
  | MessageActionValueNodes<MyData>;

function useClient(): WebSocket {
  const client = useMemo(() => new WebSocket("ws://localhost/socket"), []);
  return client;
}

function useSubscription(client: WebSocket, paths: string[]): void {
  const previousPaths = useRef<string[]>(paths);
  useEffect(() => {
    client.send(
      JSON.stringify({
        type: "SUBSCRIBE",
        paths,
      } as MessageActionSubscribe)
    );

    return (): void => {
      client.send(
        JSON.stringify({
          type: "UNSUBSCRIBE",
          paths: previousPaths.current,
        } as MessageActionUnsubscribe)
      );
      previousPaths.current = paths;
    };
  }, [client, paths]);
}

function useLatestData(client: WebSocket): MyData | undefined {
  const [latestData, setLatestData] = useState<MyData | undefined>();

  useEffect(() => {
    client.onmessage = (event): void => {
      // You could put some client-side validation here if you like
      // It's not THAT important as the server should be responsible for
      // data integrity
      const action = event.data as MessageAction;
      switch (action.type) {
        case "VALUE_NODES":
          setLatestData(action.nodes);
        default:
        // pass
      }
    };
  }, [client]);

  return latestData;
}

function useSetData(client: WebSocket): (updates: Partial<MyData>) => void {
  const setData = useCallback((updates: Partial<MyData>): void => {
    client.send(
      JSON.stringify({
        updates,
      } as MessageActionSetValues<MyData>)
    );
  }, []);

  return setData;
}

export type FetchEditHookResult<T> = {
  latestData: T | undefined;
  setData: (updates: Partial<T>) => void;
};

export function useFetchEdit(): FetchEditHookResult<MyData> {
  const client = useClient();

  useSubscription(client, ["/foo", "/bar"]);
  const latestData = useLatestData(client);
  const setData = useSetData(client);

  return { latestData, setData };
}
