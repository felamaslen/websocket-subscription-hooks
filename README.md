# hx challenge

This isn't intended to be a fully functional project, but more a proof of concept of React / WebSockets design principles.

The idea is that there is a hook which lives outside of the component logic, driving a data subscription and mutation model. This could be based on graphql (using urql) or something more complex, but for the sake of the example it is a simple and statically defined API.

The responsibility of `view.tsx` is to run the hook, and then render a component of the user's choice based on that data.
